import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSteam, faTwitch, faTwitter } from '@fortawesome/free-brands-svg-icons';
import {
  faFish,
  faGlobe,
  faBarcode,
  faLink,
  faHeart,
  faBomb,
  faCar,
  faSmile,
  faPuzzlePiece,
  faM,
  faCircleXmark
} from '@fortawesome/free-solid-svg-icons';

import {
  fetchName,
  fetchDescription,
  fetchSteamAppID,
  fetchMobygamesGameID,
  fetchGamersGlobalGameID,
  fetchOfficialWebsite,
  fetchFandomArticle,
  fetchGiantBombEntry,
  fetchIGCDgameId,
  fetchIGDBgameId,
  fetchKnowYourMeme,
  fetchLiquipediaArticle,
  fetchMetacriticID,
  fetchOpenCriticID,
  fetchTwitterUsername,
} from '../app/query';

import '../app/globals.css';

const PageWithID = () => {
  const router = useRouter();
  const { id: wikidataEntityId } = router.query;

  const [loading, setLoading] = useState(true);

  const [name, setName] = useState<string | null>(null);
  const [description, setDescription] = useState<string | null>(null);

  const [steamAppID, setSteamAppID] = useState<string | null>(null);
  const [mobygamesGameID, setMobygamesGameID] = useState<string | null>(null);
  const [gamersGlobalGameID, setGamersGlobalGameID] = useState<string | null>(null);
  const [officialWebsite, setOfficialWebsite] = useState<string | null>(null);
  const [fandomArticle, setFandomArticle] = useState<string | null>(null);
  const [giantBombEntry, setGiantBombEntry] = useState<string | null>(null);
  const [igcdGameID, setIGCDgameID] = useState<string | null>(null);
  const [igdbGameID, setIGDBgameID] = useState<string | null>(null);
  const [knowYourMemeID, setKnowYourMemeID] = useState<string | null>(null);
  const [liquipediaArticle, setLiquipediaArticle] = useState<string | null>(null);
  const [metacriticID, setMetacriticID] = useState<string | null>(null);
  const [openCriticID, setOpenCriticID] = useState<string | null>(null);
  const [twitterUsername, setTwitterUsername] = useState<string | null>(null);


  useEffect(() => {
    const getSteamAppID = async () => {
      if (!wikidataEntityId)
        return;

      setName(await fetchName(wikidataEntityId as string));
      setDescription(await fetchDescription(wikidataEntityId as string));

      setSteamAppID(await fetchSteamAppID(wikidataEntityId as string));
      setMobygamesGameID(await fetchMobygamesGameID(wikidataEntityId as string));
      setGamersGlobalGameID(await fetchGamersGlobalGameID(wikidataEntityId as string));
      setOfficialWebsite(await fetchOfficialWebsite(wikidataEntityId as string));
      setFandomArticle(await fetchFandomArticle(wikidataEntityId as string));
      setGiantBombEntry(await fetchGiantBombEntry(wikidataEntityId as string));
      setIGCDgameID(await fetchIGCDgameId(wikidataEntityId as string));
      setIGDBgameID(await fetchIGDBgameId(wikidataEntityId as string));
      setKnowYourMemeID(await fetchKnowYourMeme(wikidataEntityId as string));
      setLiquipediaArticle(await fetchLiquipediaArticle(wikidataEntityId as string));
      setMetacriticID(await fetchMetacriticID(wikidataEntityId as string));
      setOpenCriticID(await fetchOpenCriticID(wikidataEntityId as string));
      setTwitterUsername(await fetchTwitterUsername(wikidataEntityId as string));

      setLoading(false);
    };

    getSteamAppID();
  }, [wikidataEntityId]);

  return (
    <div>
      {loading ? (
        <p>Loading...</p>
      ) : (
        <>
        <h1 className="title">{name}</h1>
        <p>{description}</p>
        <p>Edit on <FontAwesomeIcon icon={faBarcode} />{' '} <a href={"https://www.wikidata.org/wiki/" + wikidataEntityId}>Wikidata</a></p>
        <h2 className="heading">Weblinks</h2>
          <ul>
            {officialWebsite && (
              <li>
                <FontAwesomeIcon icon={faLink} className="icon-indent" />{' '}
                <a href={officialWebsite}>official Website</a>
              </li>
            )}
            {steamAppID && (
              <li>
                <FontAwesomeIcon icon={faSteam} className="icon-indent" />{' '}
                <a href={'https://store.steampowered.com/app/' + steamAppID}>Steam</a>
              </li>
            )}
            {mobygamesGameID && (
              <li>
                <FontAwesomeIcon icon={faFish} className="icon-indent" />{' '}
                <a href={'https://www.mobygames.com/game/' + mobygamesGameID}>MobyGames</a>
              </li>
            )}
            {gamersGlobalGameID && (
              <li>
                <FontAwesomeIcon icon={faGlobe} className="icon-indent" />{' '}
                <a href={'https://www.gamersglobal.de/spiel/' + gamersGlobalGameID}>GamersGlobal</a>
              </li>
            )}
            {fandomArticle && (
              <li>
                <FontAwesomeIcon icon={faHeart} className="icon-indent" />{' '}
                <a href={'https://community.fandom.com/wiki/w:c:counterstrike:' + fandomArticle}>Fandom</a>
              </li>
            )}
            {giantBombEntry && (
              <li>
                <FontAwesomeIcon icon={faBomb} className="icon-indent" />{' '}
                <a href={'https://www.giantbomb.com/wd/' + giantBombEntry}>Giant Bomb</a>
              </li>
            )}
            {igcdGameID && (
              <li>
                <FontAwesomeIcon icon={faCar} className="icon-indent" />{' '}
                <a href={'https://igcd.net/game.php?id=' + igcdGameID}>IGCD</a>
              </li>
            )}
            {igdbGameID && (
              <li>
                <FontAwesomeIcon icon={faTwitch} className="icon-indent" />{' '}
                <a href={'https://www.igdb.com/games/' + igcdGameID}>IGCD</a>
              </li>
            )}
            {knowYourMemeID && (
              <li>
                <FontAwesomeIcon icon={faSmile} className="icon-indent" />{' '}
                <a href={'https://knowyourmeme.com/memes/' + knowYourMemeID}>Know Your Meme</a>
              </li>
            )}
            {liquipediaArticle && (
              <li>
                <FontAwesomeIcon icon={faPuzzlePiece} className="icon-indent" />{' '}
                <a href={'https://liquipedia.net/' + liquipediaArticle}>Liquipedia</a>
              </li>
            )}
            {metacriticID && (
              <li>
                <FontAwesomeIcon icon={faM} className="icon-indent" />{' '}
                <a href={'https://www.metacritic.com/game/' + metacriticID}>Metacritic</a>
              </li>
            )}
            {openCriticID && (
              <li>
                <FontAwesomeIcon icon={faCircleXmark} className="icon-indent" />{' '}
                <a href={`https://opencritic.com/game/${openCriticID}/-`}>OpenCritic</a>
              </li>
            )}
            {twitterUsername && (
              <li>
                <FontAwesomeIcon icon={faTwitter} className="icon-indent" />{' '}
                <a href={`https://twitter.com/${twitterUsername}`}>Twitter</a>
              </li>
            )}
          </ul>
        </>
      )}
    </div>
  );
};

export default PageWithID;
