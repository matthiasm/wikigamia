"use client";

import { useState } from 'react';

export default function Home() {
  const [entityId, setEntityId] = useState<string>('');

  const handleRedirect = () => {
    if (entityId) {
      window.location.href = `/${entityId}`;
    }
  };

  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', height: '100vh' }}>
      <img src="/images/logo.svg" alt="Logo" style={{ width: '150px', marginBottom: '20px' }} />

      <div>
        <input
          type="text"
          placeholder="Enter Wikidata ID"
          value={entityId}
          onChange={(e) => setEntityId(e.target.value)}
          style={{
            padding: '10px',
            fontSize: '16px',
            width: '250px',
            border: '1px solid #ccc',
            borderRadius: '4px',
          }}
        />
      </div>

      <div>
        <button
          onClick={handleRedirect}
          style={{
            marginTop: '20px',
            padding: '10px 20px',
            fontSize: '18px',
            backgroundColor: '#0070f3',
            color: 'white',
            border: 'none',
            cursor: 'pointer',
            borderRadius: '4px'
          }}
        >
          Go
        </button>
      </div>
    </div>
  );
}
