import axios from 'axios';
import WBK, { EntityId } from 'wikibase-sdk';

const wbk = WBK({
  instance: 'https://www.wikidata.org',
  sparqlEndpoint: 'https://query.wikidata.org/sparql',
});

export const fetchName = async (entityId: string): Promise<string | null> => {
  interface WikidataEntity {
    entities: {
      [id: string]: {
        labels: {
          [lang: string]: {
            value: string;
          };
        };
      };
    };
  }

  try {
    const url = wbk.getEntities({
      ids: [entityId as EntityId],
      props: ['labels'],
      languages: ['en'],
    });

    const response = await fetch(url);
    const data: WikidataEntity = await response.json();

    const entity = data.entities[entityId];
    return entity?.labels?.en?.value || null;

  } catch (error) {
    console.error('Error:', error);
    return null;
  }
};

export const fetchDescription = async (entityId: string): Promise<string | null> => {
  interface WikidataEntity {
    entities: {
      [id: string]: {
        descriptions: {
          [lang: string]: {
            value: string;
          };
        };
      };
    };
  }

  try {
    const url = wbk.getEntities({
      ids: [entityId as EntityId],
      props: ['descriptions'],
      languages: ['en'],
    });

    const response = await fetch(url);
    const data: WikidataEntity = await response.json();

    const entity = data.entities[entityId];
    return entity?.descriptions?.en?.value || null;

  } catch (error) {
    console.error('Error:', error);
    return null;
  }
};

const fetchWikidataID = async (wikidataEntityId: string, propertyId: string) => {
  const sparqlQuery = `
    SELECT ?externalID WHERE {
      wd:${wikidataEntityId} wdt:${propertyId} ?externalID .
    } LIMIT 1
  `;

  const url = wbk.sparqlQuery(sparqlQuery);

  try {
    const response = await axios.get(url);
    const results = response.data.results.bindings;
    if (results.length > 0) {
      return results[0]["externalID"].value;
    } else {
      return null;
    }
  } catch (error) {
    console.error(`Error fetching ${propertyId}:`, error);
    return null;
  }
};

export const fetchSteamAppID = (wikidataEntityId: string) =>
  fetchWikidataID(wikidataEntityId, 'P1733');

export const fetchMobygamesGameID = (wikidataEntityId: string) =>
  fetchWikidataID(wikidataEntityId, 'P11688');

export const fetchGamersGlobalGameID = (wikidataEntityId: string) =>
  fetchWikidataID(wikidataEntityId, 'P11771');

export const fetchOfficialWebsite = (wikidataEntityId: string) =>
  fetchWikidataID(wikidataEntityId, 'P856');

export const fetchFandomArticle = (wikidataEntityId: string) =>
  fetchWikidataID(wikidataEntityId, 'P6262');

export const fetchGiantBombEntry = (wikidataEntityId: string) =>
  fetchWikidataID(wikidataEntityId, 'P5247');

export const fetchIGCDgameId = (wikidataEntityId: string) =>
  fetchWikidataID(wikidataEntityId, 'P8018');

export const fetchIGDBgameId = (wikidataEntityId: string) =>
  fetchWikidataID(wikidataEntityId, 'P5754');

export const fetchKnowYourMeme = (wikidataEntityId: string) =>
  fetchWikidataID(wikidataEntityId, 'P6760');

export const fetchLiquipediaArticle = (wikidataEntityId: string) =>
  fetchWikidataID(wikidataEntityId, 'P10918');

export const fetchMetacriticID = (wikidataEntityId: string) =>
  fetchWikidataID(wikidataEntityId, 'P12054');

export const fetchOpenCriticID = (wikidataEntityId: string) =>
  fetchWikidataID(wikidataEntityId, 'P2864');

export const fetchTwitterUsername = (wikidataEntityId: string) =>
  fetchWikidataID(wikidataEntityId, 'P2002');
